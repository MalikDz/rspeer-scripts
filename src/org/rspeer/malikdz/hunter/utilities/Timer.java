package org.rspeer.malikdz.hunter.utilities;

/**
 * 
 * @author MalikDz
 *
 */

public class Timer {

	private long startTime, sleepingTime;

	public Timer(final long sleepingTime) {
		this.startTime = System.currentTimeMillis();
		this.sleepingTime = sleepingTime;
	}

	public long getElapsedTime() {
		return System.currentTimeMillis() - startTime;
	}

	public void reset() {
		startTime = System.currentTimeMillis();
	}

	public boolean isRunning() {
		return getElapsedTime() < sleepingTime;
	}
}
